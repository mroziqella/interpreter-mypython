/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mroziqella.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.mroziqella.domain.Tekst;
import pl.mroziqella.service.TekstService;

/**
 *
 * @author Kamil
 */
@Controller
public class controllerCompilator {

    @Autowired
    private TekstService tekstService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String kompilator(Model model) {
        Tekst newTekst = new Tekst();
        model.addAttribute("newTekst", newTekst);
        return "kompilator";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String kompiluj(Model model, Tekst tekst) {
        model.addAttribute("newTekst", tekst);
        tekstService.setTekst(tekst.getKodDoKompilacji());
        System.out.println(tekst.getKodDoKompilacji()+tekst);
        return "redirect:/wynik";
    }

    @RequestMapping("/wynik")
    public String wynik(Model model) {
        model.addAttribute("newTekst", tekstService.getWynik());
        return "wynik";
    }

}
