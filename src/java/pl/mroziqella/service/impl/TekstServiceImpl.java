/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mroziqella.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mroziqella.domain.Tekst;
import pl.mroziqella.domain.repository.TekstRepository;
import pl.mroziqella.service.TekstService;

/**
 *
 * @author Kamil
 */

@Service
public class TekstServiceImpl implements TekstService{
   
    @Autowired
    private TekstRepository tekstRepository;
    
    @Override
    public Tekst getWynik() {
       return tekstRepository.getWynik();
    }

    @Override
    public void setTekst(String tekst) {
       tekstRepository.setTekst(tekst);
    }
    
}
