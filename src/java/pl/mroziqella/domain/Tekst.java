/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mroziqella.domain;

/**
 *
 * @author Kamil
 */
public class Tekst {
    private String wynik;
    private String kodDoKompilacji;

    public Tekst() {
    }

     
    
    public Tekst(String wynik, String kodDoKompilacji) {
        this.wynik = wynik;
        this.kodDoKompilacji = kodDoKompilacji;
    }

    
    public String getWynik() {
        return wynik;
    }

    public void setWynik(String wynik) {
        this.wynik = wynik;
    }

    public String getKodDoKompilacji() {
        return kodDoKompilacji;
    }

    public void setKodDoKompilacji(String kodDoKompilacji) {
        this.kodDoKompilacji = kodDoKompilacji;
    }

    

    
}
