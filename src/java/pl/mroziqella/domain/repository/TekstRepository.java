/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mroziqella.domain.repository;

import pl.mroziqella.domain.Tekst;
/**
 *
 * @author Kamil
 */
public interface TekstRepository {
   Tekst getWynik();
   void setTekst(String tekst);
}
