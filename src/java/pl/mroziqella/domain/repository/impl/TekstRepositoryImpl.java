package pl.mroziqella.domain.repository.impl;

import org.springframework.stereotype.Repository;
import pl.mroziqella.domain.Tekst;
import pl.mroziqella.domain.repository.TekstRepository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kamil
 */
@Repository
public class TekstRepositoryImpl implements TekstRepository{
    private Tekst tekst;
    
    public TekstRepositoryImpl() {
        tekst = new Tekst();
    }

    
    @Override
    public Tekst getWynik() {
        Kompilator.Run.run(tekst.getKodDoKompilacji());
        tekst.setWynik(Kompilator.WypiszDane.getTekst());
        Kompilator.WypiszDane.wyczyscTekst();
       return tekst;
    }

    @Override
    public void setTekst(String tekst) {
       this.tekst.setKodDoKompilacji(tekst);
    }
    
}
