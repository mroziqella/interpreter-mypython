grammar Kompilator;

INT    : [0-9]+;
DOUBLE : [0-9]+'.'[0-9]+;
PI     : 'pi';
E      : 'e';
POW    : '^';
NL     : ';';
WS     : [ \n\t\r]+ -> skip;
ID     : [a-zA-Z_][a-zA-Z_0-9]*;
CZUDZYSLOW : '"';
PLUS  : '+';
EQUAL : '=';
MINUS : '-';
MULT  : '*';
DIV   : '/';
LPAR  : '(';
RPAR  : ')';



input
    :
     setVar kod NL?  # ToSetVar
    | input NL #start
    | plusOrMinus input NL?   # Calculate
    | instrukcjaWarunkowa NL?  #toSetFunction
    | petlaFor NL? #toPetlaFor
    | petlaWhile NL? #toPetlaWhile
    | print NL? #wypisz
    | println NL? #wypiszWLini

    ;

petlaFor
    :
    'for' zmiennaPetla range ':' wykonaj koniecBloku?
    ;
petlaWhile
    : 'while' LPAR warunek  RPAR ':' wykonaj koniecBloku?
    ;
zmiennaPetla
    : ID 'in'
    ;
range
    : 'range' LPAR (INT|ID) RPAR
    ;

instrukcjaWarunkowa
    : 'if' LPAR warunek  RPAR ':' wykonaj koniecBloku?
    ;
koniecBloku
    :  'end' kod ('end')*?
    ;
wykonaj: kod
;

print
    : 'print' LPAR (ID|INT|napis|plusOrMinus)? RPAR kod
    ;
println
    : 'println' LPAR (ID|INT|napis|plusOrMinus)? RPAR kod
    ;
napis
    : CZUDZYSLOW ID* CZUDZYSLOW
    ;

kod
    :(plusOrMinus|setVar|instrukcjaWarunkowa|print|println|petlaFor|petlaWhile)*
    ;
warunek
    : lewaWarunek ((znak|operator) prawaWarunek?)*
    ;
lewaWarunek
    :
    (ID|INT|DOUBLE)
    ;
prawaWarunek
    :
    (ID|INT|DOUBLE)
    ;
znak
    : '=='
     |'<='
     |'>='
     |'<'
     |'>'
     |'!=';

operator:'&';

setVar
    : ID EQUAL plusOrMinus # SetVariable
    ;


plusOrMinus 
    : plusOrMinus PLUS multOrDiv  # Plus
    | plusOrMinus MINUS multOrDiv # Minus
    | multOrDiv                   # ToMultOrDiv
    ;

multOrDiv
    : multOrDiv MULT pow # Multiplication
    | multOrDiv DIV pow  # Division
    | pow                # ToPow
    ;

pow
    : unaryMinus (POW pow)? # Power
    ;

unaryMinus
    : MINUS unaryMinus # ChangeSign
    | atom             # ToAtom
    ;

atom
    : PI                    # ConstantPI
    | E                     # ConstantE
    | DOUBLE                # Double
    | INT                   # Int
    | ID                    # Variable
    | LPAR plusOrMinus RPAR # Braces
    ;
