package Kompilator;

/**
 * Created by Kamil on 2015-12-15.
 */
public class WypiszDane {
    private static String tekst = "=============================START====================================\n";

    public static String getTekst() {
        return tekst;
    }

    public static void setTekst(String tekst) {
        WypiszDane.tekst = WypiszDane.tekst+tekst;
    }
    public static void wyczyscTekst(){
        tekst="=============================START====================================\n";
    }
}
