// Generated from C:/Users/Kamil/IdeaProjects/proj/src\Kompilator.g4 by ANTLR 4.5.1
package Kompilator;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class KompilatorParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, INT=17, 
		DOUBLE=18, PI=19, E=20, POW=21, NL=22, WS=23, ID=24, CZUDZYSLOW=25, PLUS=26, 
		EQUAL=27, MINUS=28, MULT=29, DIV=30, LPAR=31, RPAR=32;
	public static final int
		RULE_input = 0, RULE_petlaFor = 1, RULE_petlaWhile = 2, RULE_zmiennaPetla = 3, 
		RULE_range = 4, RULE_instrukcjaWarunkowa = 5, RULE_koniecBloku = 6, RULE_wykonaj = 7, 
		RULE_print = 8, RULE_println = 9, RULE_napis = 10, RULE_kod = 11, RULE_warunek = 12, 
		RULE_lewaWarunek = 13, RULE_prawaWarunek = 14, RULE_znak = 15, RULE_operator = 16, 
		RULE_setVar = 17, RULE_plusOrMinus = 18, RULE_multOrDiv = 19, RULE_pow = 20, 
		RULE_unaryMinus = 21, RULE_atom = 22;
	public static final String[] ruleNames = {
		"input", "petlaFor", "petlaWhile", "zmiennaPetla", "range", "instrukcjaWarunkowa", 
		"koniecBloku", "wykonaj", "print", "println", "napis", "kod", "warunek", 
		"lewaWarunek", "prawaWarunek", "znak", "operator", "setVar", "plusOrMinus", 
		"multOrDiv", "pow", "unaryMinus", "atom"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'for'", "':'", "'while'", "'in'", "'range'", "'if'", "'end'", "'print'", 
		"'println'", "'=='", "'<='", "'>='", "'<'", "'>'", "'!='", "'&'", null, 
		null, "'pi'", "'e'", "'^'", "';'", null, null, "'\"'", "'+'", "'='", "'-'", 
		"'*'", "'/'", "'('", "')'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "INT", "DOUBLE", "PI", "E", "POW", "NL", 
		"WS", "ID", "CZUDZYSLOW", "PLUS", "EQUAL", "MINUS", "MULT", "DIV", "LPAR", 
		"RPAR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Kompilator.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public KompilatorParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class InputContext extends ParserRuleContext {
		public InputContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_input; }
	 
		public InputContext() { }
		public void copyFrom(InputContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class WypiszWLiniContext extends InputContext {
		public PrintlnContext println() {
			return getRuleContext(PrintlnContext.class,0);
		}
		public TerminalNode NL() { return getToken(KompilatorParser.NL, 0); }
		public WypiszWLiniContext(InputContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitWypiszWLini(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ToPetlaForContext extends InputContext {
		public PetlaForContext petlaFor() {
			return getRuleContext(PetlaForContext.class,0);
		}
		public TerminalNode NL() { return getToken(KompilatorParser.NL, 0); }
		public ToPetlaForContext(InputContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitToPetlaFor(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StartContext extends InputContext {
		public InputContext input() {
			return getRuleContext(InputContext.class,0);
		}
		public TerminalNode NL() { return getToken(KompilatorParser.NL, 0); }
		public StartContext(InputContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitStart(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CalculateContext extends InputContext {
		public PlusOrMinusContext plusOrMinus() {
			return getRuleContext(PlusOrMinusContext.class,0);
		}
		public InputContext input() {
			return getRuleContext(InputContext.class,0);
		}
		public TerminalNode NL() { return getToken(KompilatorParser.NL, 0); }
		public CalculateContext(InputContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitCalculate(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WypiszContext extends InputContext {
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public TerminalNode NL() { return getToken(KompilatorParser.NL, 0); }
		public WypiszContext(InputContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitWypisz(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ToSetVarContext extends InputContext {
		public SetVarContext setVar() {
			return getRuleContext(SetVarContext.class,0);
		}
		public KodContext kod() {
			return getRuleContext(KodContext.class,0);
		}
		public TerminalNode NL() { return getToken(KompilatorParser.NL, 0); }
		public ToSetVarContext(InputContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitToSetVar(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ToPetlaWhileContext extends InputContext {
		public PetlaWhileContext petlaWhile() {
			return getRuleContext(PetlaWhileContext.class,0);
		}
		public TerminalNode NL() { return getToken(KompilatorParser.NL, 0); }
		public ToPetlaWhileContext(InputContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitToPetlaWhile(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ToSetFunctionContext extends InputContext {
		public InstrukcjaWarunkowaContext instrukcjaWarunkowa() {
			return getRuleContext(InstrukcjaWarunkowaContext.class,0);
		}
		public TerminalNode NL() { return getToken(KompilatorParser.NL, 0); }
		public ToSetFunctionContext(InputContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitToSetFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InputContext input() throws RecognitionException {
		return input(0);
	}

	private InputContext input(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		InputContext _localctx = new InputContext(_ctx, _parentState);
		InputContext _prevctx = _localctx;
		int _startState = 0;
		enterRecursionRule(_localctx, 0, RULE_input, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(77);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				{
				_localctx = new ToSetVarContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(47);
				setVar();
				setState(48);
				kod();
				setState(50);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					setState(49);
					match(NL);
					}
					break;
				}
				}
				break;
			case 2:
				{
				_localctx = new CalculateContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(52);
				plusOrMinus(0);
				setState(53);
				input(0);
				setState(55);
				switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
				case 1:
					{
					setState(54);
					match(NL);
					}
					break;
				}
				}
				break;
			case 3:
				{
				_localctx = new ToSetFunctionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(57);
				instrukcjaWarunkowa();
				setState(59);
				switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
				case 1:
					{
					setState(58);
					match(NL);
					}
					break;
				}
				}
				break;
			case 4:
				{
				_localctx = new ToPetlaForContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(61);
				petlaFor();
				setState(63);
				switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
				case 1:
					{
					setState(62);
					match(NL);
					}
					break;
				}
				}
				break;
			case 5:
				{
				_localctx = new ToPetlaWhileContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(65);
				petlaWhile();
				setState(67);
				switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
				case 1:
					{
					setState(66);
					match(NL);
					}
					break;
				}
				}
				break;
			case 6:
				{
				_localctx = new WypiszContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(69);
				print();
				setState(71);
				switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
				case 1:
					{
					setState(70);
					match(NL);
					}
					break;
				}
				}
				break;
			case 7:
				{
				_localctx = new WypiszWLiniContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(73);
				println();
				setState(75);
				switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
				case 1:
					{
					setState(74);
					match(NL);
					}
					break;
				}
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(83);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new StartContext(new InputContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_input);
					setState(79);
					if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
					setState(80);
					match(NL);
					}
					} 
				}
				setState(85);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PetlaForContext extends ParserRuleContext {
		public ZmiennaPetlaContext zmiennaPetla() {
			return getRuleContext(ZmiennaPetlaContext.class,0);
		}
		public RangeContext range() {
			return getRuleContext(RangeContext.class,0);
		}
		public WykonajContext wykonaj() {
			return getRuleContext(WykonajContext.class,0);
		}
		public KoniecBlokuContext koniecBloku() {
			return getRuleContext(KoniecBlokuContext.class,0);
		}
		public PetlaForContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_petlaFor; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitPetlaFor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PetlaForContext petlaFor() throws RecognitionException {
		PetlaForContext _localctx = new PetlaForContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_petlaFor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(86);
			match(T__0);
			setState(87);
			zmiennaPetla();
			setState(88);
			range();
			setState(89);
			match(T__1);
			setState(90);
			wykonaj();
			setState(92);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(91);
				koniecBloku();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PetlaWhileContext extends ParserRuleContext {
		public TerminalNode LPAR() { return getToken(KompilatorParser.LPAR, 0); }
		public WarunekContext warunek() {
			return getRuleContext(WarunekContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(KompilatorParser.RPAR, 0); }
		public WykonajContext wykonaj() {
			return getRuleContext(WykonajContext.class,0);
		}
		public KoniecBlokuContext koniecBloku() {
			return getRuleContext(KoniecBlokuContext.class,0);
		}
		public PetlaWhileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_petlaWhile; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitPetlaWhile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PetlaWhileContext petlaWhile() throws RecognitionException {
		PetlaWhileContext _localctx = new PetlaWhileContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_petlaWhile);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94);
			match(T__2);
			setState(95);
			match(LPAR);
			setState(96);
			warunek();
			setState(97);
			match(RPAR);
			setState(98);
			match(T__1);
			setState(99);
			wykonaj();
			setState(101);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				{
				setState(100);
				koniecBloku();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ZmiennaPetlaContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(KompilatorParser.ID, 0); }
		public ZmiennaPetlaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_zmiennaPetla; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitZmiennaPetla(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ZmiennaPetlaContext zmiennaPetla() throws RecognitionException {
		ZmiennaPetlaContext _localctx = new ZmiennaPetlaContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_zmiennaPetla);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(103);
			match(ID);
			setState(104);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RangeContext extends ParserRuleContext {
		public TerminalNode LPAR() { return getToken(KompilatorParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(KompilatorParser.RPAR, 0); }
		public TerminalNode INT() { return getToken(KompilatorParser.INT, 0); }
		public TerminalNode ID() { return getToken(KompilatorParser.ID, 0); }
		public RangeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_range; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitRange(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RangeContext range() throws RecognitionException {
		RangeContext _localctx = new RangeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_range);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(106);
			match(T__4);
			setState(107);
			match(LPAR);
			setState(108);
			_la = _input.LA(1);
			if ( !(_la==INT || _la==ID) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(109);
			match(RPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstrukcjaWarunkowaContext extends ParserRuleContext {
		public TerminalNode LPAR() { return getToken(KompilatorParser.LPAR, 0); }
		public WarunekContext warunek() {
			return getRuleContext(WarunekContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(KompilatorParser.RPAR, 0); }
		public WykonajContext wykonaj() {
			return getRuleContext(WykonajContext.class,0);
		}
		public KoniecBlokuContext koniecBloku() {
			return getRuleContext(KoniecBlokuContext.class,0);
		}
		public InstrukcjaWarunkowaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instrukcjaWarunkowa; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitInstrukcjaWarunkowa(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstrukcjaWarunkowaContext instrukcjaWarunkowa() throws RecognitionException {
		InstrukcjaWarunkowaContext _localctx = new InstrukcjaWarunkowaContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_instrukcjaWarunkowa);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			match(T__5);
			setState(112);
			match(LPAR);
			setState(113);
			warunek();
			setState(114);
			match(RPAR);
			setState(115);
			match(T__1);
			setState(116);
			wykonaj();
			setState(118);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				setState(117);
				koniecBloku();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KoniecBlokuContext extends ParserRuleContext {
		public KodContext kod() {
			return getRuleContext(KodContext.class,0);
		}
		public KoniecBlokuContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_koniecBloku; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitKoniecBloku(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KoniecBlokuContext koniecBloku() throws RecognitionException {
		KoniecBlokuContext _localctx = new KoniecBlokuContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_koniecBloku);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(120);
			match(T__6);
			setState(121);
			kod();
			setState(125);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1+1 ) {
					{
					{
					setState(122);
					match(T__6);
					}
					} 
				}
				setState(127);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WykonajContext extends ParserRuleContext {
		public KodContext kod() {
			return getRuleContext(KodContext.class,0);
		}
		public WykonajContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wykonaj; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitWykonaj(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WykonajContext wykonaj() throws RecognitionException {
		WykonajContext _localctx = new WykonajContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_wykonaj);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(128);
			kod();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintContext extends ParserRuleContext {
		public TerminalNode LPAR() { return getToken(KompilatorParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(KompilatorParser.RPAR, 0); }
		public KodContext kod() {
			return getRuleContext(KodContext.class,0);
		}
		public TerminalNode ID() { return getToken(KompilatorParser.ID, 0); }
		public TerminalNode INT() { return getToken(KompilatorParser.INT, 0); }
		public NapisContext napis() {
			return getRuleContext(NapisContext.class,0);
		}
		public PlusOrMinusContext plusOrMinus() {
			return getRuleContext(PlusOrMinusContext.class,0);
		}
		public PrintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintContext print() throws RecognitionException {
		PrintContext _localctx = new PrintContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_print);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(130);
			match(T__7);
			setState(131);
			match(LPAR);
			setState(136);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				{
				setState(132);
				match(ID);
				}
				break;
			case 2:
				{
				setState(133);
				match(INT);
				}
				break;
			case 3:
				{
				setState(134);
				napis();
				}
				break;
			case 4:
				{
				setState(135);
				plusOrMinus(0);
				}
				break;
			}
			setState(138);
			match(RPAR);
			setState(139);
			kod();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintlnContext extends ParserRuleContext {
		public TerminalNode LPAR() { return getToken(KompilatorParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(KompilatorParser.RPAR, 0); }
		public KodContext kod() {
			return getRuleContext(KodContext.class,0);
		}
		public TerminalNode ID() { return getToken(KompilatorParser.ID, 0); }
		public TerminalNode INT() { return getToken(KompilatorParser.INT, 0); }
		public NapisContext napis() {
			return getRuleContext(NapisContext.class,0);
		}
		public PlusOrMinusContext plusOrMinus() {
			return getRuleContext(PlusOrMinusContext.class,0);
		}
		public PrintlnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_println; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitPrintln(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintlnContext println() throws RecognitionException {
		PrintlnContext _localctx = new PrintlnContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_println);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(141);
			match(T__8);
			setState(142);
			match(LPAR);
			setState(147);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(143);
				match(ID);
				}
				break;
			case 2:
				{
				setState(144);
				match(INT);
				}
				break;
			case 3:
				{
				setState(145);
				napis();
				}
				break;
			case 4:
				{
				setState(146);
				plusOrMinus(0);
				}
				break;
			}
			setState(149);
			match(RPAR);
			setState(150);
			kod();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NapisContext extends ParserRuleContext {
		public List<TerminalNode> CZUDZYSLOW() { return getTokens(KompilatorParser.CZUDZYSLOW); }
		public TerminalNode CZUDZYSLOW(int i) {
			return getToken(KompilatorParser.CZUDZYSLOW, i);
		}
		public List<TerminalNode> ID() { return getTokens(KompilatorParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(KompilatorParser.ID, i);
		}
		public NapisContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_napis; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitNapis(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NapisContext napis() throws RecognitionException {
		NapisContext _localctx = new NapisContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_napis);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(152);
			match(CZUDZYSLOW);
			setState(156);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID) {
				{
				{
				setState(153);
				match(ID);
				}
				}
				setState(158);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(159);
			match(CZUDZYSLOW);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KodContext extends ParserRuleContext {
		public List<PlusOrMinusContext> plusOrMinus() {
			return getRuleContexts(PlusOrMinusContext.class);
		}
		public PlusOrMinusContext plusOrMinus(int i) {
			return getRuleContext(PlusOrMinusContext.class,i);
		}
		public List<SetVarContext> setVar() {
			return getRuleContexts(SetVarContext.class);
		}
		public SetVarContext setVar(int i) {
			return getRuleContext(SetVarContext.class,i);
		}
		public List<InstrukcjaWarunkowaContext> instrukcjaWarunkowa() {
			return getRuleContexts(InstrukcjaWarunkowaContext.class);
		}
		public InstrukcjaWarunkowaContext instrukcjaWarunkowa(int i) {
			return getRuleContext(InstrukcjaWarunkowaContext.class,i);
		}
		public List<PrintContext> print() {
			return getRuleContexts(PrintContext.class);
		}
		public PrintContext print(int i) {
			return getRuleContext(PrintContext.class,i);
		}
		public List<PrintlnContext> println() {
			return getRuleContexts(PrintlnContext.class);
		}
		public PrintlnContext println(int i) {
			return getRuleContext(PrintlnContext.class,i);
		}
		public List<PetlaForContext> petlaFor() {
			return getRuleContexts(PetlaForContext.class);
		}
		public PetlaForContext petlaFor(int i) {
			return getRuleContext(PetlaForContext.class,i);
		}
		public List<PetlaWhileContext> petlaWhile() {
			return getRuleContexts(PetlaWhileContext.class);
		}
		public PetlaWhileContext petlaWhile(int i) {
			return getRuleContext(PetlaWhileContext.class,i);
		}
		public KodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kod; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitKod(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KodContext kod() throws RecognitionException {
		KodContext _localctx = new KodContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_kod);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					setState(168);
					switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
					case 1:
						{
						setState(161);
						plusOrMinus(0);
						}
						break;
					case 2:
						{
						setState(162);
						setVar();
						}
						break;
					case 3:
						{
						setState(163);
						instrukcjaWarunkowa();
						}
						break;
					case 4:
						{
						setState(164);
						print();
						}
						break;
					case 5:
						{
						setState(165);
						println();
						}
						break;
					case 6:
						{
						setState(166);
						petlaFor();
						}
						break;
					case 7:
						{
						setState(167);
						petlaWhile();
						}
						break;
					}
					} 
				}
				setState(172);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WarunekContext extends ParserRuleContext {
		public LewaWarunekContext lewaWarunek() {
			return getRuleContext(LewaWarunekContext.class,0);
		}
		public List<ZnakContext> znak() {
			return getRuleContexts(ZnakContext.class);
		}
		public ZnakContext znak(int i) {
			return getRuleContext(ZnakContext.class,i);
		}
		public List<OperatorContext> operator() {
			return getRuleContexts(OperatorContext.class);
		}
		public OperatorContext operator(int i) {
			return getRuleContext(OperatorContext.class,i);
		}
		public List<PrawaWarunekContext> prawaWarunek() {
			return getRuleContexts(PrawaWarunekContext.class);
		}
		public PrawaWarunekContext prawaWarunek(int i) {
			return getRuleContext(PrawaWarunekContext.class,i);
		}
		public WarunekContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_warunek; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitWarunek(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WarunekContext warunek() throws RecognitionException {
		WarunekContext _localctx = new WarunekContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_warunek);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(173);
			lewaWarunek();
			setState(183);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15))) != 0)) {
				{
				{
				setState(176);
				switch (_input.LA(1)) {
				case T__9:
				case T__10:
				case T__11:
				case T__12:
				case T__13:
				case T__14:
					{
					setState(174);
					znak();
					}
					break;
				case T__15:
					{
					setState(175);
					operator();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(179);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT) | (1L << DOUBLE) | (1L << ID))) != 0)) {
					{
					setState(178);
					prawaWarunek();
					}
				}

				}
				}
				setState(185);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LewaWarunekContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(KompilatorParser.ID, 0); }
		public TerminalNode INT() { return getToken(KompilatorParser.INT, 0); }
		public TerminalNode DOUBLE() { return getToken(KompilatorParser.DOUBLE, 0); }
		public LewaWarunekContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lewaWarunek; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitLewaWarunek(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LewaWarunekContext lewaWarunek() throws RecognitionException {
		LewaWarunekContext _localctx = new LewaWarunekContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_lewaWarunek);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT) | (1L << DOUBLE) | (1L << ID))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrawaWarunekContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(KompilatorParser.ID, 0); }
		public TerminalNode INT() { return getToken(KompilatorParser.INT, 0); }
		public TerminalNode DOUBLE() { return getToken(KompilatorParser.DOUBLE, 0); }
		public PrawaWarunekContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prawaWarunek; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitPrawaWarunek(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrawaWarunekContext prawaWarunek() throws RecognitionException {
		PrawaWarunekContext _localctx = new PrawaWarunekContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_prawaWarunek);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(188);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT) | (1L << DOUBLE) | (1L << ID))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ZnakContext extends ParserRuleContext {
		public ZnakContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_znak; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitZnak(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ZnakContext znak() throws RecognitionException {
		ZnakContext _localctx = new ZnakContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_znak);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperatorContext extends ParserRuleContext {
		public OperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operator; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperatorContext operator() throws RecognitionException {
		OperatorContext _localctx = new OperatorContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_operator);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(T__15);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetVarContext extends ParserRuleContext {
		public SetVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setVar; }
	 
		public SetVarContext() { }
		public void copyFrom(SetVarContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SetVariableContext extends SetVarContext {
		public TerminalNode ID() { return getToken(KompilatorParser.ID, 0); }
		public TerminalNode EQUAL() { return getToken(KompilatorParser.EQUAL, 0); }
		public PlusOrMinusContext plusOrMinus() {
			return getRuleContext(PlusOrMinusContext.class,0);
		}
		public SetVariableContext(SetVarContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitSetVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetVarContext setVar() throws RecognitionException {
		SetVarContext _localctx = new SetVarContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_setVar);
		try {
			_localctx = new SetVariableContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(194);
			match(ID);
			setState(195);
			match(EQUAL);
			setState(196);
			plusOrMinus(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PlusOrMinusContext extends ParserRuleContext {
		public PlusOrMinusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plusOrMinus; }
	 
		public PlusOrMinusContext() { }
		public void copyFrom(PlusOrMinusContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ToMultOrDivContext extends PlusOrMinusContext {
		public MultOrDivContext multOrDiv() {
			return getRuleContext(MultOrDivContext.class,0);
		}
		public ToMultOrDivContext(PlusOrMinusContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitToMultOrDiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PlusContext extends PlusOrMinusContext {
		public PlusOrMinusContext plusOrMinus() {
			return getRuleContext(PlusOrMinusContext.class,0);
		}
		public TerminalNode PLUS() { return getToken(KompilatorParser.PLUS, 0); }
		public MultOrDivContext multOrDiv() {
			return getRuleContext(MultOrDivContext.class,0);
		}
		public PlusContext(PlusOrMinusContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitPlus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MinusContext extends PlusOrMinusContext {
		public PlusOrMinusContext plusOrMinus() {
			return getRuleContext(PlusOrMinusContext.class,0);
		}
		public TerminalNode MINUS() { return getToken(KompilatorParser.MINUS, 0); }
		public MultOrDivContext multOrDiv() {
			return getRuleContext(MultOrDivContext.class,0);
		}
		public MinusContext(PlusOrMinusContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitMinus(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PlusOrMinusContext plusOrMinus() throws RecognitionException {
		return plusOrMinus(0);
	}

	private PlusOrMinusContext plusOrMinus(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PlusOrMinusContext _localctx = new PlusOrMinusContext(_ctx, _parentState);
		PlusOrMinusContext _prevctx = _localctx;
		int _startState = 36;
		enterRecursionRule(_localctx, 36, RULE_plusOrMinus, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new ToMultOrDivContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(199);
			multOrDiv(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(209);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(207);
					switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
					case 1:
						{
						_localctx = new PlusContext(new PlusOrMinusContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_plusOrMinus);
						setState(201);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(202);
						match(PLUS);
						setState(203);
						multOrDiv(0);
						}
						break;
					case 2:
						{
						_localctx = new MinusContext(new PlusOrMinusContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_plusOrMinus);
						setState(204);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(205);
						match(MINUS);
						setState(206);
						multOrDiv(0);
						}
						break;
					}
					} 
				}
				setState(211);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class MultOrDivContext extends ParserRuleContext {
		public MultOrDivContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multOrDiv; }
	 
		public MultOrDivContext() { }
		public void copyFrom(MultOrDivContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class MultiplicationContext extends MultOrDivContext {
		public MultOrDivContext multOrDiv() {
			return getRuleContext(MultOrDivContext.class,0);
		}
		public TerminalNode MULT() { return getToken(KompilatorParser.MULT, 0); }
		public PowContext pow() {
			return getRuleContext(PowContext.class,0);
		}
		public MultiplicationContext(MultOrDivContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitMultiplication(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DivisionContext extends MultOrDivContext {
		public MultOrDivContext multOrDiv() {
			return getRuleContext(MultOrDivContext.class,0);
		}
		public TerminalNode DIV() { return getToken(KompilatorParser.DIV, 0); }
		public PowContext pow() {
			return getRuleContext(PowContext.class,0);
		}
		public DivisionContext(MultOrDivContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitDivision(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ToPowContext extends MultOrDivContext {
		public PowContext pow() {
			return getRuleContext(PowContext.class,0);
		}
		public ToPowContext(MultOrDivContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitToPow(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultOrDivContext multOrDiv() throws RecognitionException {
		return multOrDiv(0);
	}

	private MultOrDivContext multOrDiv(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		MultOrDivContext _localctx = new MultOrDivContext(_ctx, _parentState);
		MultOrDivContext _prevctx = _localctx;
		int _startState = 38;
		enterRecursionRule(_localctx, 38, RULE_multOrDiv, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new ToPowContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(213);
			pow();
			}
			_ctx.stop = _input.LT(-1);
			setState(223);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(221);
					switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
					case 1:
						{
						_localctx = new MultiplicationContext(new MultOrDivContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_multOrDiv);
						setState(215);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(216);
						match(MULT);
						setState(217);
						pow();
						}
						break;
					case 2:
						{
						_localctx = new DivisionContext(new MultOrDivContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_multOrDiv);
						setState(218);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(219);
						match(DIV);
						setState(220);
						pow();
						}
						break;
					}
					} 
				}
				setState(225);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PowContext extends ParserRuleContext {
		public PowContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pow; }
	 
		public PowContext() { }
		public void copyFrom(PowContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PowerContext extends PowContext {
		public UnaryMinusContext unaryMinus() {
			return getRuleContext(UnaryMinusContext.class,0);
		}
		public TerminalNode POW() { return getToken(KompilatorParser.POW, 0); }
		public PowContext pow() {
			return getRuleContext(PowContext.class,0);
		}
		public PowerContext(PowContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitPower(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PowContext pow() throws RecognitionException {
		PowContext _localctx = new PowContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_pow);
		try {
			_localctx = new PowerContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(226);
			unaryMinus();
			setState(229);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				{
				setState(227);
				match(POW);
				setState(228);
				pow();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryMinusContext extends ParserRuleContext {
		public UnaryMinusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryMinus; }
	 
		public UnaryMinusContext() { }
		public void copyFrom(UnaryMinusContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ChangeSignContext extends UnaryMinusContext {
		public TerminalNode MINUS() { return getToken(KompilatorParser.MINUS, 0); }
		public UnaryMinusContext unaryMinus() {
			return getRuleContext(UnaryMinusContext.class,0);
		}
		public ChangeSignContext(UnaryMinusContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitChangeSign(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ToAtomContext extends UnaryMinusContext {
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public ToAtomContext(UnaryMinusContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitToAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryMinusContext unaryMinus() throws RecognitionException {
		UnaryMinusContext _localctx = new UnaryMinusContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_unaryMinus);
		try {
			setState(234);
			switch (_input.LA(1)) {
			case MINUS:
				_localctx = new ChangeSignContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(231);
				match(MINUS);
				setState(232);
				unaryMinus();
				}
				break;
			case INT:
			case DOUBLE:
			case PI:
			case E:
			case ID:
			case LPAR:
				_localctx = new ToAtomContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(233);
				atom();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
	 
		public AtomContext() { }
		public void copyFrom(AtomContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ConstantPIContext extends AtomContext {
		public TerminalNode PI() { return getToken(KompilatorParser.PI, 0); }
		public ConstantPIContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitConstantPI(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableContext extends AtomContext {
		public TerminalNode ID() { return getToken(KompilatorParser.ID, 0); }
		public VariableContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BracesContext extends AtomContext {
		public TerminalNode LPAR() { return getToken(KompilatorParser.LPAR, 0); }
		public PlusOrMinusContext plusOrMinus() {
			return getRuleContext(PlusOrMinusContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(KompilatorParser.RPAR, 0); }
		public BracesContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitBraces(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConstantEContext extends AtomContext {
		public TerminalNode E() { return getToken(KompilatorParser.E, 0); }
		public ConstantEContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitConstantE(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DoubleContext extends AtomContext {
		public TerminalNode DOUBLE() { return getToken(KompilatorParser.DOUBLE, 0); }
		public DoubleContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitDouble(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntContext extends AtomContext {
		public TerminalNode INT() { return getToken(KompilatorParser.INT, 0); }
		public IntContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof KompilatorVisitor ) return ((KompilatorVisitor<? extends T>)visitor).visitInt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_atom);
		try {
			setState(245);
			switch (_input.LA(1)) {
			case PI:
				_localctx = new ConstantPIContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(236);
				match(PI);
				}
				break;
			case E:
				_localctx = new ConstantEContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(237);
				match(E);
				}
				break;
			case DOUBLE:
				_localctx = new DoubleContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(238);
				match(DOUBLE);
				}
				break;
			case INT:
				_localctx = new IntContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(239);
				match(INT);
				}
				break;
			case ID:
				_localctx = new VariableContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(240);
				match(ID);
				}
				break;
			case LPAR:
				_localctx = new BracesContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(241);
				match(LPAR);
				setState(242);
				plusOrMinus(0);
				setState(243);
				match(RPAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 0:
			return input_sempred((InputContext)_localctx, predIndex);
		case 18:
			return plusOrMinus_sempred((PlusOrMinusContext)_localctx, predIndex);
		case 19:
			return multOrDiv_sempred((MultOrDivContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean input_sempred(InputContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 7);
		}
		return true;
	}
	private boolean plusOrMinus_sempred(PlusOrMinusContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 3);
		case 2:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean multOrDiv_sempred(MultOrDivContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 3);
		case 4:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\"\u00fa\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2\3\2\3"+
		"\2\3\2\5\2\65\n\2\3\2\3\2\3\2\5\2:\n\2\3\2\3\2\5\2>\n\2\3\2\3\2\5\2B\n"+
		"\2\3\2\3\2\5\2F\n\2\3\2\3\2\5\2J\n\2\3\2\3\2\5\2N\n\2\5\2P\n\2\3\2\3\2"+
		"\7\2T\n\2\f\2\16\2W\13\2\3\3\3\3\3\3\3\3\3\3\3\3\5\3_\n\3\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\5\4h\n\4\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\5\7y\n\7\3\b\3\b\3\b\7\b~\n\b\f\b\16\b\u0081\13\b\3\t"+
		"\3\t\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u008b\n\n\3\n\3\n\3\n\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\5\13\u0096\n\13\3\13\3\13\3\13\3\f\3\f\7\f\u009d\n\f\f"+
		"\f\16\f\u00a0\13\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\7\r\u00ab\n\r\f"+
		"\r\16\r\u00ae\13\r\3\16\3\16\3\16\5\16\u00b3\n\16\3\16\5\16\u00b6\n\16"+
		"\7\16\u00b8\n\16\f\16\16\16\u00bb\13\16\3\17\3\17\3\20\3\20\3\21\3\21"+
		"\3\22\3\22\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\7\24\u00d2\n\24\f\24\16\24\u00d5\13\24\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\7\25\u00e0\n\25\f\25\16\25\u00e3\13\25\3\26\3\26"+
		"\3\26\5\26\u00e8\n\26\3\27\3\27\3\27\5\27\u00ed\n\27\3\30\3\30\3\30\3"+
		"\30\3\30\3\30\3\30\3\30\3\30\5\30\u00f8\n\30\3\30\3\177\5\2&(\31\2\4\6"+
		"\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\2\5\4\2\23\23\32\32\4\2\23"+
		"\24\32\32\3\2\f\21\u0112\2O\3\2\2\2\4X\3\2\2\2\6`\3\2\2\2\bi\3\2\2\2\n"+
		"l\3\2\2\2\fq\3\2\2\2\16z\3\2\2\2\20\u0082\3\2\2\2\22\u0084\3\2\2\2\24"+
		"\u008f\3\2\2\2\26\u009a\3\2\2\2\30\u00ac\3\2\2\2\32\u00af\3\2\2\2\34\u00bc"+
		"\3\2\2\2\36\u00be\3\2\2\2 \u00c0\3\2\2\2\"\u00c2\3\2\2\2$\u00c4\3\2\2"+
		"\2&\u00c8\3\2\2\2(\u00d6\3\2\2\2*\u00e4\3\2\2\2,\u00ec\3\2\2\2.\u00f7"+
		"\3\2\2\2\60\61\b\2\1\2\61\62\5$\23\2\62\64\5\30\r\2\63\65\7\30\2\2\64"+
		"\63\3\2\2\2\64\65\3\2\2\2\65P\3\2\2\2\66\67\5&\24\2\679\5\2\2\28:\7\30"+
		"\2\298\3\2\2\29:\3\2\2\2:P\3\2\2\2;=\5\f\7\2<>\7\30\2\2=<\3\2\2\2=>\3"+
		"\2\2\2>P\3\2\2\2?A\5\4\3\2@B\7\30\2\2A@\3\2\2\2AB\3\2\2\2BP\3\2\2\2CE"+
		"\5\6\4\2DF\7\30\2\2ED\3\2\2\2EF\3\2\2\2FP\3\2\2\2GI\5\22\n\2HJ\7\30\2"+
		"\2IH\3\2\2\2IJ\3\2\2\2JP\3\2\2\2KM\5\24\13\2LN\7\30\2\2ML\3\2\2\2MN\3"+
		"\2\2\2NP\3\2\2\2O\60\3\2\2\2O\66\3\2\2\2O;\3\2\2\2O?\3\2\2\2OC\3\2\2\2"+
		"OG\3\2\2\2OK\3\2\2\2PU\3\2\2\2QR\f\t\2\2RT\7\30\2\2SQ\3\2\2\2TW\3\2\2"+
		"\2US\3\2\2\2UV\3\2\2\2V\3\3\2\2\2WU\3\2\2\2XY\7\3\2\2YZ\5\b\5\2Z[\5\n"+
		"\6\2[\\\7\4\2\2\\^\5\20\t\2]_\5\16\b\2^]\3\2\2\2^_\3\2\2\2_\5\3\2\2\2"+
		"`a\7\5\2\2ab\7!\2\2bc\5\32\16\2cd\7\"\2\2de\7\4\2\2eg\5\20\t\2fh\5\16"+
		"\b\2gf\3\2\2\2gh\3\2\2\2h\7\3\2\2\2ij\7\32\2\2jk\7\6\2\2k\t\3\2\2\2lm"+
		"\7\7\2\2mn\7!\2\2no\t\2\2\2op\7\"\2\2p\13\3\2\2\2qr\7\b\2\2rs\7!\2\2s"+
		"t\5\32\16\2tu\7\"\2\2uv\7\4\2\2vx\5\20\t\2wy\5\16\b\2xw\3\2\2\2xy\3\2"+
		"\2\2y\r\3\2\2\2z{\7\t\2\2{\177\5\30\r\2|~\7\t\2\2}|\3\2\2\2~\u0081\3\2"+
		"\2\2\177\u0080\3\2\2\2\177}\3\2\2\2\u0080\17\3\2\2\2\u0081\177\3\2\2\2"+
		"\u0082\u0083\5\30\r\2\u0083\21\3\2\2\2\u0084\u0085\7\n\2\2\u0085\u008a"+
		"\7!\2\2\u0086\u008b\7\32\2\2\u0087\u008b\7\23\2\2\u0088\u008b\5\26\f\2"+
		"\u0089\u008b\5&\24\2\u008a\u0086\3\2\2\2\u008a\u0087\3\2\2\2\u008a\u0088"+
		"\3\2\2\2\u008a\u0089\3\2\2\2\u008a\u008b\3\2\2\2\u008b\u008c\3\2\2\2\u008c"+
		"\u008d\7\"\2\2\u008d\u008e\5\30\r\2\u008e\23\3\2\2\2\u008f\u0090\7\13"+
		"\2\2\u0090\u0095\7!\2\2\u0091\u0096\7\32\2\2\u0092\u0096\7\23\2\2\u0093"+
		"\u0096\5\26\f\2\u0094\u0096\5&\24\2\u0095\u0091\3\2\2\2\u0095\u0092\3"+
		"\2\2\2\u0095\u0093\3\2\2\2\u0095\u0094\3\2\2\2\u0095\u0096\3\2\2\2\u0096"+
		"\u0097\3\2\2\2\u0097\u0098\7\"\2\2\u0098\u0099\5\30\r\2\u0099\25\3\2\2"+
		"\2\u009a\u009e\7\33\2\2\u009b\u009d\7\32\2\2\u009c\u009b\3\2\2\2\u009d"+
		"\u00a0\3\2\2\2\u009e\u009c\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a1\3\2"+
		"\2\2\u00a0\u009e\3\2\2\2\u00a1\u00a2\7\33\2\2\u00a2\27\3\2\2\2\u00a3\u00ab"+
		"\5&\24\2\u00a4\u00ab\5$\23\2\u00a5\u00ab\5\f\7\2\u00a6\u00ab\5\22\n\2"+
		"\u00a7\u00ab\5\24\13\2\u00a8\u00ab\5\4\3\2\u00a9\u00ab\5\6\4\2\u00aa\u00a3"+
		"\3\2\2\2\u00aa\u00a4\3\2\2\2\u00aa\u00a5\3\2\2\2\u00aa\u00a6\3\2\2\2\u00aa"+
		"\u00a7\3\2\2\2\u00aa\u00a8\3\2\2\2\u00aa\u00a9\3\2\2\2\u00ab\u00ae\3\2"+
		"\2\2\u00ac\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\31\3\2\2\2\u00ae\u00ac"+
		"\3\2\2\2\u00af\u00b9\5\34\17\2\u00b0\u00b3\5 \21\2\u00b1\u00b3\5\"\22"+
		"\2\u00b2\u00b0\3\2\2\2\u00b2\u00b1\3\2\2\2\u00b3\u00b5\3\2\2\2\u00b4\u00b6"+
		"\5\36\20\2\u00b5\u00b4\3\2\2\2\u00b5\u00b6\3\2\2\2\u00b6\u00b8\3\2\2\2"+
		"\u00b7\u00b2\3\2\2\2\u00b8\u00bb\3\2\2\2\u00b9\u00b7\3\2\2\2\u00b9\u00ba"+
		"\3\2\2\2\u00ba\33\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bc\u00bd\t\3\2\2\u00bd"+
		"\35\3\2\2\2\u00be\u00bf\t\3\2\2\u00bf\37\3\2\2\2\u00c0\u00c1\t\4\2\2\u00c1"+
		"!\3\2\2\2\u00c2\u00c3\7\22\2\2\u00c3#\3\2\2\2\u00c4\u00c5\7\32\2\2\u00c5"+
		"\u00c6\7\35\2\2\u00c6\u00c7\5&\24\2\u00c7%\3\2\2\2\u00c8\u00c9\b\24\1"+
		"\2\u00c9\u00ca\5(\25\2\u00ca\u00d3\3\2\2\2\u00cb\u00cc\f\5\2\2\u00cc\u00cd"+
		"\7\34\2\2\u00cd\u00d2\5(\25\2\u00ce\u00cf\f\4\2\2\u00cf\u00d0\7\36\2\2"+
		"\u00d0\u00d2\5(\25\2\u00d1\u00cb\3\2\2\2\u00d1\u00ce\3\2\2\2\u00d2\u00d5"+
		"\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\'\3\2\2\2\u00d5"+
		"\u00d3\3\2\2\2\u00d6\u00d7\b\25\1\2\u00d7\u00d8\5*\26\2\u00d8\u00e1\3"+
		"\2\2\2\u00d9\u00da\f\5\2\2\u00da\u00db\7\37\2\2\u00db\u00e0\5*\26\2\u00dc"+
		"\u00dd\f\4\2\2\u00dd\u00de\7 \2\2\u00de\u00e0\5*\26\2\u00df\u00d9\3\2"+
		"\2\2\u00df\u00dc\3\2\2\2\u00e0\u00e3\3\2\2\2\u00e1\u00df\3\2\2\2\u00e1"+
		"\u00e2\3\2\2\2\u00e2)\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e4\u00e7\5,\27\2"+
		"\u00e5\u00e6\7\27\2\2\u00e6\u00e8\5*\26\2\u00e7\u00e5\3\2\2\2\u00e7\u00e8"+
		"\3\2\2\2\u00e8+\3\2\2\2\u00e9\u00ea\7\36\2\2\u00ea\u00ed\5,\27\2\u00eb"+
		"\u00ed\5.\30\2\u00ec\u00e9\3\2\2\2\u00ec\u00eb\3\2\2\2\u00ed-\3\2\2\2"+
		"\u00ee\u00f8\7\25\2\2\u00ef\u00f8\7\26\2\2\u00f0\u00f8\7\24\2\2\u00f1"+
		"\u00f8\7\23\2\2\u00f2\u00f8\7\32\2\2\u00f3\u00f4\7!\2\2\u00f4\u00f5\5"+
		"&\24\2\u00f5\u00f6\7\"\2\2\u00f6\u00f8\3\2\2\2\u00f7\u00ee\3\2\2\2\u00f7"+
		"\u00ef\3\2\2\2\u00f7\u00f0\3\2\2\2\u00f7\u00f1\3\2\2\2\u00f7\u00f2\3\2"+
		"\2\2\u00f7\u00f3\3\2\2\2\u00f8/\3\2\2\2\36\649=AEIMOU^gx\177\u008a\u0095"+
		"\u009e\u00aa\u00ac\u00b2\u00b5\u00b9\u00d1\u00d3\u00df\u00e1\u00e7\u00ec"+
		"\u00f7";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}