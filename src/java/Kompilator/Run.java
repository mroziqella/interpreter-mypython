package Kompilator;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.util.Scanner;

public class Run {
    
     public static void run(String tekst){
         ANTLRInputStream input = new ANTLRInputStream(tekst);

        KompilatorLexer lexer = new KompilatorLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        KompilatorParser parser = new KompilatorParser(tokens);
        ParseTree tree = parser.input();

        KompilatorBaseVisitorImpl calcVisitor = new KompilatorBaseVisitorImpl();
        Double result = calcVisitor.visit(tree);
        //System.out.println("Wynik: " + result);
        System.out.println("Zakończono Powodzeniem!!!");
    }
     
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
       //ANTLRInputStream input = new ANTLRFileStream("C:\\Users\\Kamil\\IdeaProjects\\proj\\src\\input");
        ANTLRInputStream input = new ANTLRInputStream("print(5+2)");

        KompilatorLexer lexer = new KompilatorLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        KompilatorParser parser = new KompilatorParser(tokens);
        ParseTree tree = parser.input();

        KompilatorBaseVisitorImpl calcVisitor = new KompilatorBaseVisitorImpl();
        Double result = calcVisitor.visit(tree);
        //System.out.println("Wynik: " + result);
        System.out.println("Zakończono Powodzeniem!!!");
    }
}
