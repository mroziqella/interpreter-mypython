// Generated from C:/Users/Kamil/IdeaProjects/proj/src\Kompilator.g4 by ANTLR 4.5.1
package Kompilator;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link KompilatorParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface KompilatorVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code wypiszWLini}
	 * labeled alternative in {@link KompilatorParser#input}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWypiszWLini(KompilatorParser.WypiszWLiniContext ctx);
	/**
	 * Visit a parse tree produced by the {@code toPetlaFor}
	 * labeled alternative in {@link KompilatorParser#input}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToPetlaFor(KompilatorParser.ToPetlaForContext ctx);
	/**
	 * Visit a parse tree produced by the {@code start}
	 * labeled alternative in {@link KompilatorParser#input}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(KompilatorParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Calculate}
	 * labeled alternative in {@link KompilatorParser#input}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCalculate(KompilatorParser.CalculateContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wypisz}
	 * labeled alternative in {@link KompilatorParser#input}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWypisz(KompilatorParser.WypiszContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ToSetVar}
	 * labeled alternative in {@link KompilatorParser#input}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToSetVar(KompilatorParser.ToSetVarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code toPetlaWhile}
	 * labeled alternative in {@link KompilatorParser#input}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToPetlaWhile(KompilatorParser.ToPetlaWhileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code toSetFunction}
	 * labeled alternative in {@link KompilatorParser#input}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToSetFunction(KompilatorParser.ToSetFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#petlaFor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPetlaFor(KompilatorParser.PetlaForContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#petlaWhile}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPetlaWhile(KompilatorParser.PetlaWhileContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#zmiennaPetla}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitZmiennaPetla(KompilatorParser.ZmiennaPetlaContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#range}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRange(KompilatorParser.RangeContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#instrukcjaWarunkowa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstrukcjaWarunkowa(KompilatorParser.InstrukcjaWarunkowaContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#koniecBloku}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKoniecBloku(KompilatorParser.KoniecBlokuContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#wykonaj}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWykonaj(KompilatorParser.WykonajContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(KompilatorParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#println}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrintln(KompilatorParser.PrintlnContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#napis}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNapis(KompilatorParser.NapisContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#kod}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKod(KompilatorParser.KodContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#warunek}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWarunek(KompilatorParser.WarunekContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#lewaWarunek}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLewaWarunek(KompilatorParser.LewaWarunekContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#prawaWarunek}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrawaWarunek(KompilatorParser.PrawaWarunekContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#znak}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitZnak(KompilatorParser.ZnakContext ctx);
	/**
	 * Visit a parse tree produced by {@link KompilatorParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperator(KompilatorParser.OperatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SetVariable}
	 * labeled alternative in {@link KompilatorParser#setVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetVariable(KompilatorParser.SetVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ToMultOrDiv}
	 * labeled alternative in {@link KompilatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToMultOrDiv(KompilatorParser.ToMultOrDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link KompilatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlus(KompilatorParser.PlusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Minus}
	 * labeled alternative in {@link KompilatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinus(KompilatorParser.MinusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Multiplication}
	 * labeled alternative in {@link KompilatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplication(KompilatorParser.MultiplicationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Division}
	 * labeled alternative in {@link KompilatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivision(KompilatorParser.DivisionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ToPow}
	 * labeled alternative in {@link KompilatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToPow(KompilatorParser.ToPowContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Power}
	 * labeled alternative in {@link KompilatorParser#pow}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPower(KompilatorParser.PowerContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ChangeSign}
	 * labeled alternative in {@link KompilatorParser#unaryMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChangeSign(KompilatorParser.ChangeSignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ToAtom}
	 * labeled alternative in {@link KompilatorParser#unaryMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToAtom(KompilatorParser.ToAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ConstantPI}
	 * labeled alternative in {@link KompilatorParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstantPI(KompilatorParser.ConstantPIContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ConstantE}
	 * labeled alternative in {@link KompilatorParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstantE(KompilatorParser.ConstantEContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Double}
	 * labeled alternative in {@link KompilatorParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDouble(KompilatorParser.DoubleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Int}
	 * labeled alternative in {@link KompilatorParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInt(KompilatorParser.IntContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Variable}
	 * labeled alternative in {@link KompilatorParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(KompilatorParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Braces}
	 * labeled alternative in {@link KompilatorParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBraces(KompilatorParser.BracesContext ctx);
}