package Kompilator;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;
import java.util.HashMap;

public class KompilatorBaseVisitorImpl extends KompilatorBaseVisitor<Double> {
    private HashMap<String, Double> variables;

    public KompilatorBaseVisitorImpl() {
        variables = new HashMap<String, Double>();
    }

    @Override
    public Double visitPrint(KompilatorParser.PrintContext ctx) {
        if(ctx.ID()!=null) {
            System.out.print(" " + variables.get(ctx.ID().toString()));
            WypiszDane.setTekst(" "+variables.get(ctx.ID().toString())+"");
        }
        if(ctx.napis()!=null) {
            for (TerminalNode x : ctx.napis().ID()) {
                System.out.print(" " + x.toString() + " ");
                WypiszDane.setTekst(" "+x.toString()+" ");
            }
        }
        if(ctx.plusOrMinus()!=null){
            System.out.print(" " + visit(ctx.plusOrMinus()));
            WypiszDane.setTekst(" "+visit(ctx.plusOrMinus())+"");
        }
        return super.visitPrint(ctx);


    }

    @Override
    public Double visitPrintln(KompilatorParser.PrintlnContext ctx) {
        if(ctx.ID()!=null) {
            System.out.println(variables.get(ctx.ID().toString()));
            WypiszDane.setTekst(variables.get(ctx.ID().toString())+"\n");
        }
        if(ctx.napis()!=null) {
            for (TerminalNode x : ctx.napis().ID()) {
                System.out.print(x.toString() + " ");
                WypiszDane.setTekst(x.toString()+" ");
            }
            System.out.print("\n");
            WypiszDane.setTekst("\n");
        }
        if(ctx.plusOrMinus()!=null){
            System.out.println(visit(ctx.plusOrMinus()));
            WypiszDane.setTekst(visit(ctx.plusOrMinus())+"\n");
        }
        return super.visitPrintln(ctx);
    }

    @Override
    public Double visitWykonaj(KompilatorParser.WykonajContext ctx) {
        return super.visitWykonaj(ctx);
    }

    @Override
    public Double visitKoniecBloku(KompilatorParser.KoniecBlokuContext ctx) {
        visit(ctx.kod());
        return 0.0;
    }

    @Override
    public Double visitPetlaFor(KompilatorParser.PetlaForContext ctx) {
        Double iterator =0.0;
        int range;
        if(ctx.range().INT()==null)
            range = variables.get(ctx.range().ID().getText()).intValue();
        else
            range = Integer.parseInt(ctx.range().INT().getText());
        visit(ctx.zmiennaPetla());
        while (iterator<range) {
            visit(ctx.wykonaj());
            iterator=variables.get(ctx.zmiennaPetla().ID().getText());
            iterator+=1;
            variables.put(ctx.zmiennaPetla().ID().getText(),iterator);
        }
        variables.put(ctx.zmiennaPetla().ID().getText(),0.0);
        visit(ctx.koniecBloku());
        return 0.0;
    }

    @Override
    public Double visitZmiennaPetla(KompilatorParser.ZmiennaPetlaContext ctx) {
        variables.put(ctx.ID().getText(), 0.0);
        return 0.0;
    }

    @Override
    public Double visitInstrukcjaWarunkowa(KompilatorParser.InstrukcjaWarunkowaContext ctx) {
        final double prawda=1.0;
        if(visit(ctx.warunek())==prawda) {
            super.visitInstrukcjaWarunkowa(ctx);
        }
        else if (ctx.koniecBloku()!=null)
            visit(ctx.koniecBloku());
        return 0.0;
    }

    @Override
    public Double visitPetlaWhile(KompilatorParser.PetlaWhileContext ctx) {
        final double prawda=1.0;
        while(visit(ctx.warunek())==prawda) {
            visit(ctx.wykonaj());
        }
        if (ctx.koniecBloku()!=null)
            visit(ctx.koniecBloku());
        return 0.0;

    }

    @Override
    public Double visitWarunek(KompilatorParser.WarunekContext ctx) {
        return sprawdzWarunek(ctx);
    }

    @Override
    public Double visitPlus(@NotNull KompilatorParser.PlusContext ctx) {
        return visit(ctx.plusOrMinus()) + visit(ctx.multOrDiv());
    }


    @Override
    public Double visitMinus(@NotNull KompilatorParser.MinusContext ctx) {
        return visit(ctx.plusOrMinus()) - visit(ctx.multOrDiv());
    }

    @Override
    public Double visitToAtom(KompilatorParser.ToAtomContext ctx) {
        return super.visitToAtom(ctx);
    }

    @Override
    public Double visitMultiplication(@NotNull KompilatorParser.MultiplicationContext ctx) {
        return visit(ctx.multOrDiv()) * visit(ctx.pow());
    }

    @Override
    public Double visitDivision(@NotNull KompilatorParser.DivisionContext ctx) {
        return visit(ctx.multOrDiv()) / visit(ctx.pow());
    }

    @Override
    public Double visitSetVariable(@NotNull KompilatorParser.SetVariableContext ctx) {
        Double value = visit(ctx.plusOrMinus());
        variables.put(ctx.ID().getText(), value);
       // System.out.println(value);
        return value;
    }

    @Override
    public Double visitPower(@NotNull KompilatorParser.PowerContext ctx) {
        if (ctx.pow() != null)
            return Math.pow(visit(ctx.unaryMinus()), visit(ctx.pow()));
        return visit(ctx.unaryMinus());
    }

    @Override
    public Double visitChangeSign(@NotNull KompilatorParser.ChangeSignContext ctx) {
        return -1*visit(ctx.unaryMinus());
    }

    @Override
    public Double visitBraces(@NotNull KompilatorParser.BracesContext ctx) {
        return visit(ctx.plusOrMinus());
    }

    @Override
    public Double visitConstantPI(@NotNull KompilatorParser.ConstantPIContext ctx) {
        return Math.PI;
    }

    @Override
    public Double visitConstantE(@NotNull KompilatorParser.ConstantEContext ctx) {
        return Math.E;
    }

    @Override
    public Double visitInt(@NotNull KompilatorParser.IntContext ctx) {
        return Double.parseDouble(ctx.INT().getText());
    }

    @Override
    public Double visitVariable(@NotNull KompilatorParser.VariableContext ctx) {

        return variables.get(ctx.ID().getText());
    }

    @Override
    public Double visitDouble(@NotNull KompilatorParser.DoubleContext ctx) {

        return Double.parseDouble(ctx.DOUBLE().getText());
    }

    @Override
    public Double visitCalculate(@NotNull KompilatorParser.CalculateContext ctx) {

        return visit(ctx.plusOrMinus());
    }
    private Double sprawdzWarunek(KompilatorParser.WarunekContext ctx){
        double lewaLiczba,prawaLiczba;
        double tab[];
        tab=zwrocLewaPrawaLiczbe(ctx);
        lewaLiczba=tab[0];
        prawaLiczba=tab[1];

        if(ctx.znak().get(0).getText().equals("<")) {
            if (lewaLiczba < prawaLiczba) {
                return 1.0;
            }
        }
        if(ctx.znak().get(0).getText().equals("<=")) {
            if (lewaLiczba <= prawaLiczba) {
                return 1.0;
            }
        }
        if(ctx.znak().get(0).getText().equals(">")) {
            if (lewaLiczba > prawaLiczba) {
                return 1.0;
            }
        }
        if(ctx.znak().get(0).getText().equals(">=")) {
            if (lewaLiczba > prawaLiczba) {
                return 1.0;
            }
        }
        if(ctx.znak().get(0).getText().equals("==")) {
            if (lewaLiczba == prawaLiczba) {
                return 1.0;
            }
        }
        if(ctx.znak().get(0).getText().equals("!=")) {
            if (lewaLiczba != prawaLiczba) {
                return 1.0;
            }
        }
        return 0.0;
    }
    private double[] zwrocLewaPrawaLiczbe(KompilatorParser.WarunekContext ctx){
        double lewaLiczba,prawaLiczba;


        if(ctx.lewaWarunek().INT()!=null)
            lewaLiczba = Double.parseDouble(ctx.lewaWarunek().INT().getText());
        else if(ctx.lewaWarunek().DOUBLE()!=null)
                lewaLiczba=Double.parseDouble(ctx.lewaWarunek().DOUBLE().getText());
            else
                lewaLiczba=variables.get(ctx.lewaWarunek().ID().toString());
        if(ctx.prawaWarunek(0).INT()!=null)
            prawaLiczba= Double.parseDouble(ctx.prawaWarunek(0).INT().getText());
        else if (ctx.prawaWarunek(0).DOUBLE()!=null)
            prawaLiczba= Double.parseDouble(ctx.prawaWarunek(0).DOUBLE().getText());
            else
                prawaLiczba=variables.get(ctx.prawaWarunek(0).ID().toString());

        return new double[]{lewaLiczba,prawaLiczba};
    }
}

