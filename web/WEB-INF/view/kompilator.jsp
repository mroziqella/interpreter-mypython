<%-- 
    Document   : kompilator
    Created on : 2016-01-08, 21:12:18
    Author     : Kamil
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		<title>Kompilator</title>
	</head>
	<body>
		<section>
			<div class="jumbotron">
				<div class="container">
					<h1> Kompilator języka Python </h1>	
                                        <p>Techniki Kompilacji
                                            <br>autorzy: Mróz Kamil, Wielgus Sławomir</p>
				</div>
			</div>
		</section>
		<section class="container">
			<form:form modelAttribute="newTekst" class="from-horizontal">
				<fieldset>
					<legend>Kod do kompilacji</legend>
			
					<div class="form-grup">
						<label class="control-label col-lg-2" for="kodDoKompilacji">Kod</label>
						<div class="col-lg-10">
                                                    <form:textarea id="description" path="kodDoKompilacji" rows="20" cols="100"/>
						</div>
					</div>
					
					<div class="form-grup">
						<div class="col-lg-offset-2 col-lg-10">
							<input type="submit" id="btnAdd" class="btn btn-primary" value="Kompiluj"/>
						</div>
					</div>
				</fieldset>
			</form:form>
		</section>
	</body>
</html>